/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grupo2
 */
public class DataBaseQueries {
    
    private static Connection conn;
    private static Statement st;
    private static ResultSet resultado;
    private static String user = "root";
    private static String url = "jdbc:mysql://localhost/electroshop_3ms";
    private static String pass = "Lenovo1";
    
    public static void insertUsuario(Usuario usuario) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO usuarios VALUES("
                    + null + ", "
                    + "\"" + usuario.getNombre() + "\", "
                    + "\"" + usuario.getApellidos() + "\", "
                    + "\"" + usuario.getDireccion() + "\", "
                    + "\"" + usuario.getTelefono() + "\", "
                    + "\"" + usuario.getMail()+ "\")";
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public static ArrayList<Producto> selectProductos() throws SQLException, ClassNotFoundException{
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String sql = "SELECT nombre, precio, imagen FROM productos";
            st = conn.createStatement();
            resultado = st.executeQuery(sql);
            ArrayList<Producto> productos = new ArrayList<>();
            while(resultado.next()){
                Producto producto = new Producto(resultado.getString("nombre"), resultado.getInt("precio"), resultado.getString("imagen"));
                productos.add(producto);
            }
            conn.close();
            return productos;
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(DataBaseQueries.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static void insertProducto(Producto producto) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "INSERT INTO productos VALUES("
                    + null + ", "
                    + "\"" + producto.getNombre() + "\", "
                    + "\"" + producto.getPrecio() + "\", "
                    + "\"" + producto.getImagen()+ "\")";
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
      
    public static void updateProducto(Producto producto, int id) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "UPDATE productos SET "
                    + "nombre = \'" + producto.getNombre()
                    + "',precio = " + producto.getPrecio()
                    + ",imagen = \'" + producto.getImagen()
                    + "\' WHERE id = " + id ;
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
    public static void deleteProducto(int id) throws SQLException, ClassNotFoundException{
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, user, pass);
            String query = "DELETE FROM productos WHERE id = " + id;
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
    
}
